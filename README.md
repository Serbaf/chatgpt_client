# Documentación

Librería para comunicación con ChatGPT. Proporciona dos modos de interacción básicos:

- simple: envía cualquier texto a la API de ChatGPT y devuelve una respuesta
- interactive: abre un diálogo interactivo en el que el historial se almacena y se
  utiliza para proporcionar un contexto a la conversación.

## Modelos recomendados
Actualmente los mejores modelos en relación calidad precio son **gpt-4-1106-preview**
(mejor, más caro) y **gpt-3.5-turbo-1106** (peor, más barato).
