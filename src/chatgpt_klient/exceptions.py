class InvalidAPIKeyError(Exception):
    pass

class InvalidModelError(Exception):
    pass

class InvalidResponseError(Exception):
    pass